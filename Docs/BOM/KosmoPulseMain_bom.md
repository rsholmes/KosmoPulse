# KosmoPulseMain.kicad_sch BOM

Sun 17 Dec 2023 09:36:04 AM EST

Generated from schematic by Eeschema 7.0.9-7.0.9~ubuntu22.04.1

**Component Count:** 49

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C1 | 1 | 100nF | Ceramic capacitor, 2.5 mm pitch | Tayda |  |
| D1 | 1 | LED7 | Light emitting diode | Tayda | A-1553 |
| D2 | 1 | LED4 | Light emitting diode | Tayda | A-1553 |
| D3 | 1 | LED2 | Light emitting diode | Tayda | A-1553 |
| D4 | 1 | LED1 | Light emitting diode | Tayda | A-1553 |
| D5 | 1 | LED3 | Light emitting diode | Tayda | A-1553 |
| D6 | 1 | LED6 | Light emitting diode | Tayda | A-1553 |
| D7 | 1 | LED5 | Light emitting diode | Tayda | A-1553 |
| D8 | 1 | LED12 | Light emitting diode | Tayda | A-1553 |
| D9 | 1 | LED24 | Light emitting diode | Tayda | A-1553 |
| D10 | 1 | LED1247 | Light emitting diode | Tayda | A-1553 |
| D11 | 1 | LED47 | Light emitting diode | Tayda | A-1553 |
| J1 | 1 | OUT6 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J2 | 1 | OUT4 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J3 | 1 | OUT2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J4 | 1 | OUT1 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J5 | 1 | OUT3 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J6 | 1 | OUT7 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J7 | 1 | OUT5 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J8 | 1 | 1247 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J9 | 1 | OUT47 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J10 | 1 | OUT12 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J11 | 1 | OUT24 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J12 | 1 | Conn_01x03 | Generic connector, single row, 01x03, script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |
| PULSES1 | 1 | MA08-2 | Generic connector, double row, 02x08, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |
| R1–6, R13, R15–18 | 11 | 1K | Resistor | Tayda |  |
| R7–12, R14, R19–22 | 11 | RL | Resistor | Tayda |  |
| SW1 | 1 | SW_SPST | Single Pole Single Throw (SPST) switch | Tayda | A-3186 (SPDT, use as SPST) |
| U1 | 1 | 4081 | Quad And 2 inputs |  |  |

## Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1K|11|R1–6, R13, R15–18|
|RL|11|R7–12, R14, R19–22|

