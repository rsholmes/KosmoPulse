# HP Gates

Music Thing Modular [Turing Pulse Expander](https://github.com/TomWhitwell/Turing-Pulse-Expander) converted to Kosmo format "KosmoPulse" [design](https://github.com/BenRufenacht/KosmoPulse) by Ben Rufenacht, renamed to "HP Gates" and design revised by Rich Holmes. 

[Original schematic](https://github.com/TomWhitwell/Turing-Pulse-Expander/blob/master/Collateral/pulses_rev2_schematic.pdf)

License:  
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/) 

# Design changes

My changes to Rufenacht's version are entirely stylistic:

* Panel converted to AO style
* Panel layout revised
* Changed to slotted hole footprints for jacks
* Changed LED resistor values to "RL"
* Changed PCB and panel labels

There are a lot of LEDs here, 11 of them to be precise. Taking Vf ~ 3 V, a 2.2kΩ series resistor and a supply voltage of 12 V means about 4 mA current per LED — if half of them are lit that's ~20 mA but it varies as the LEDs light up or not. That kind of varying load can put some noticeable fluctuations on the power rail. Modern super bright LEDs are too blinding to use at the same current, but instead of "super bright" you can consider them "super low current", providing equal illumination at much smaller current; I've found, for instance, green LEDs that work well at 12 V with a series resistance of 150kΩ, for a current draw of around 60 µA per LED. Using "RL" for the resistor values makes clear which footprints are affected by a change to brighter LEDs (or a difference in individual brightness preference).

# Use

Whether the module is called "Gates" or "Pulses", it can connect to either `GATES1` or `PULSES1` on the Halting Problem. If connected to `GATES1`, `on` gates will stay on the full length of at least one clock pulse or longer. If connected to `PULSES1`, `on` gates will stay on for only half a clock pulse before going low:

![](Images/gvp.png)

Optionally, you can use the daughterboard from Clarionut's [LPG/Mix](https://github.com/clarionut/TM_LPG-MIX) design to switch between connection to `GATES1` and `PULSES1`. There are two versions of the front panel, with and without a hole for the switch. There isn't room, however, for another jack, so switching using a gate isn't feasible. One terminal of the switch is to connect to the switch connector on the daughterboard. The other switch terminal, and the daughterboard CV connector, should connect to ground. Note this modification to this module has NOT yet been tested.

## Current draw
1 mA +12 V, 1 mA -12 V

## Photos

![hpgates](Images/hpgates.jpg)

## Documentation

* [My schematic](Docs/KosmoPulseMain.pdf)
* PCB layout: [front](Docs/KosmoPulseMain_layout_front.pdf), [back](Docs/KosmoPulseMain_layout_back.pdf)
* [BOM](Docs/KosmoPulseMain_bom.md)

## Git repository

* [https://gitlab.com/rsholmes/KosmoPulse](https://gitlab.com/rsholmes/KosmoPulse)


